import React from 'react';


import { useGetAndSortProductsData } from './useGetAndSortProductsData';
import { ProductsTable } from './ProductsTable';

export function ListContainer () {
  const {
    store,
    productsSorter,
    searchProductsByName,
    rowsPerPageAction,
    changeListPage
  } = useGetAndSortProductsData();

  return (
    <ProductsTable
      count={store.count}
      data={store.data}
      order={store.order}
      limit={store.limit}
      page={store.page}
      rowsPerPage={store.rowsPerPage}
      productsSorter={productsSorter}
      searchProductsByName={searchProductsByName}
      rowsPerPageAction={rowsPerPageAction}
      changeListPage={changeListPage}
    />
  );
}