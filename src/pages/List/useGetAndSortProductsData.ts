import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getProductsData, sortProducts, searchByName, changeRowsPerPage, changePage } from './actions';
import { State } from './types';

type Store = {
  listState: State,
};

type HookReturn = {
  store: State,
  productsSorter: Function,
  searchProductsByName: Function,
  rowsPerPageAction: Function,
  changeListPage: Function
};

export function useGetAndSortProductsData(): HookReturn {
  const dispatch = useDispatch();
  const store = useSelector((store: Store) => store.listState);

  const productsSorter = (field: 'id'|'quantity'|'date') => {
    sortProducts(store.order, field, dispatch);
  };

  const rowsPerPageAction = (rowsPerPage: number) => {
    changeRowsPerPage(rowsPerPage, dispatch);
  };

  const searchProductsByName = (text: string) => {
    searchByName(text, dispatch);
  };

  const changeListPage = (newPage: number) => {
    changePage(newPage, store.limit, store.skip, store.rowsPerPage, store.page, dispatch);
  };

  useEffect(() => {
    getProductsData({
      search: store.search,
      order: store.order,
      skip: store.skip,
      limit: store.limit,
      dispatch: dispatch
    });
  }, [store.search, store.order.direction, store.order.field, store.skip, store.limit]);

  return { store, productsSorter, searchProductsByName, rowsPerPageAction, changeListPage };
}