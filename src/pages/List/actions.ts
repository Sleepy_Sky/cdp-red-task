import { fetcher } from '../../utils/fetcher';
import { API_PRODUCTS, API_PRODUCTS_COUNT } from '../../config/endpoints';
import {
  GET_PRODUCTS_START,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCTS_ERROR,
  SORT_PRODUCTS,
  SEARCH_PRODUCT_BY_NAME,
  CHANGE_ROWS_PER_PAGE,
  CHANGE_PAGE
} from '../../actionsTypes';
import { Order, GetProductsDataArguments, OrderField } from './types';

export function changePage(newPage: number, limit: number, skip: number, rowsPerPage: number, page: number, dispatch: Function) {
  const newLimit = page < newPage ? limit + rowsPerPage : limit - rowsPerPage;
  const newSkip = page < newPage ? skip + rowsPerPage : skip - rowsPerPage;

  dispatch({
    type: CHANGE_PAGE,
    value: {
      page: newPage,
      limit: newLimit,
      skip: newSkip
    }
  });
}

export function changeRowsPerPage(rowsPerPage: number, dispatch: Function) {
  dispatch({
    type: CHANGE_ROWS_PER_PAGE,
    value: {
      rowsPerPage,
      limit: rowsPerPage,
      skip: 0
    }
  });
}

export function searchByName(text: string, dispatch: Function) {
  dispatch({
    type: SEARCH_PRODUCT_BY_NAME,
    value: text
  });
}

export function sortProducts(prevState: Order, field: OrderField, dispatch: Function) {
  if (field === prevState.field) {
    const newDirection = prevState.direction === 'asc' ? 'desc' : 'asc';

    dispatch({
      type: SORT_PRODUCTS,
      value: { field: prevState.field, direction: newDirection }
    })
  } else {
    dispatch({
      type: SORT_PRODUCTS,
      value: { field: field, direction: 'asc' }
    })
  }
}

export async function getProductsData(args: GetProductsDataArguments) {
  const { dispatch, order, limit, search, skip } = args;

  const isSearch = !!search ? { where: { name: search } } : undefined;

  const filterToString = JSON.stringify({
    order: `${order.field} ${order.direction}`,
    limit,
    skip,
    ...isSearch
  });

  const urlWithFilter = `${API_PRODUCTS}?filter=${encodeURIComponent(filterToString)}`;

  try {
    dispatch({
      type: GET_PRODUCTS_START,
      value: {
        loading: true
      }
    });

    const response = await fetcher.get({ url: urlWithFilter });
    const countObj = await fetcher.get({ url: API_PRODUCTS_COUNT });

    dispatch({
      type: GET_PRODUCTS_SUCCESS,
      value: {
        loading: false,
        success: true,
        data: response,
        count: countObj.count
      }
    });
  } catch (error) {
    dispatch({
      type: GET_PRODUCTS_ERROR,
      value: {
        loading: false,
        error: true,
        errorMessage: error.message
      }
    });
  }
}