import React, { FunctionComponent }  from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import Moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import TablePagination from '@material-ui/core/TablePagination';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import InputBase from '@material-ui/core/InputBase';
import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';
import Search from '@material-ui/icons/Search';
import { AutoSizer } from 'react-virtualized';

import { RowData, Order } from './types';

type Props = {
  data: Array<RowData>,
  order: Order,
  productsSorter: Function,
  searchProductsByName: Function,
  count: number,
  rowsPerPageAction: Function,
  limit: number,
  changeListPage: Function,
  page: number,
  rowsPerPage: number
};

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    width: '100%',
    boxSizing: 'border-box',
    paddingTop: theme.spacing(),
    overflow: 'auto',
    display: 'grid',
    gridTemplateRows: '45px 1fr',
    alignItems: 'start',
    flexShrink: 0
  },
  toolbar: {
    height: '100%',
    width: '100%',
    paddingRight: theme.spacing(),
    paddingLeft: theme.spacing(),
    display: 'grid',
    gridTemplateColumns: '1fr 150px',
    alignItem: 'center',
    justifyContent: 'center'
  },
  search: {
    backgroundColor: theme.palette.grey[200],
    borderRadius: theme.shape.borderRadius,
    paddingRight: theme.spacing(),
    paddingLeft: theme.spacing(),
    maxWidth: '400px',
    display: 'flex',
    alignItems: 'center'
  },
  tableContainer: {
    height: '100%'
  }
}));

export function ProductsTable(props: Props) {
  const {
    data,
    order,
    productsSorter,
    searchProductsByName,
    count,
    rowsPerPageAction,
    limit,
    changeListPage,
    page,
    rowsPerPage
  } = props;
  const classes = useStyles();
  const match = useRouteMatch();

  return (
    <AutoSizer>
      {({ height, width }) => (
        <Paper className={classes.root} style={{ height: height, width: width }}>
          <div className={classes.toolbar}>
            <div className={classes.search}>
              <InputBase
                onChange={e => searchProductsByName(e.target.value)}
                fullWidth
                placeholder="Search by product name"
              />
              <Search />
            </div>
            <Button
              variant="outlined"
              color="primary"
              component={Link}
              to={`${match.url}/add`}
            >
              Add Product
            </Button>
          </div>
          <TableContainer className={classes.tableContainer}>
            <Table stickyHeader size="medium">
              <TableHead>
                <TableRow>
                  <TableCell>
                    <TableSortLabel
                      onClick={() => productsSorter('id')}
                      active={order.field === 'id'}
                      direction={order.direction}
                    >
                      ID
                    </TableSortLabel>
                  </TableCell>
                  <TableCell>Name</TableCell>
                  <TableCell>
                    <TableSortLabel
                      onClick={() => productsSorter('quantity')}
                      active={order.field === 'quantity'}
                      direction={order.direction}
                    >
                      Quantity
                    </TableSortLabel>
                  </TableCell>
                  <TableCell>
                    <TableSortLabel
                      onClick={() => productsSorter('date')}
                      active={order.field === 'date'}
                      direction={order.direction}
                    >
                      Date
                    </TableSortLabel>
                  </TableCell>
                  <TableCell>Description</TableCell>
                  <TableCell>Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map(element => (
                  <TableRow key={element.id}>
                    <TableCell>{element.id}</TableCell>
                    <TableCell>{element.name}</TableCell>
                    <TableCell>{element.quantity}</TableCell>
                    <TableCell>{Moment(element.date).format('DD.MM.YYYY')}</TableCell>
                    <TableCell>{element.description}</TableCell>
                    <TableCell>
                      <ButtonGroup size="small">
                        <Button
                          component={Link}
                          to={`${match.url}/edit/${element.id}`}
                          title="Edit"
                        >
                          <Edit />
                        </Button>
                        <Button
                          component={Link}
                          to={`${match.url}/delete/${element.id}`}
                          title="Delete"
                        >
                          <Delete />
                        </Button>
                      </ButtonGroup>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            onChangePage={(e, newPage) => changeListPage(newPage)}
            component="div"
            count={count}
            rowsPerPage={rowsPerPage}
            page={page}
            rowsPerPageOptions={[10, 20, 30]}
            onChangeRowsPerPage={e => rowsPerPageAction(e.target.value)}
          />
        </Paper>
      )}
    </AutoSizer>
  );
}
