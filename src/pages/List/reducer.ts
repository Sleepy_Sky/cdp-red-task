import {
  GET_PRODUCTS_START,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCTS_ERROR,
  SORT_PRODUCTS,
  SEARCH_PRODUCT_BY_NAME,
  CHANGE_ROWS_PER_PAGE,
  CHANGE_PAGE
} from '../../actionsTypes';

import { State, Action } from './types';

const DEFAULT_STATE = {
  loading: false,
  success: false,
  error: false,
  data: [],
  order: { field: 'id' as 'id', direction: 'asc' as 'asc' },
  limit: 10,
  skip: 0,
  count: 0,
  rowsPerPage: 10,
  page: 0
};

export function listState(state: State = DEFAULT_STATE, action: Action = {}) {
  switch (action.type) {
    case GET_PRODUCTS_START:
    case GET_PRODUCTS_SUCCESS:
    case GET_PRODUCTS_ERROR:
    case CHANGE_ROWS_PER_PAGE:
    case CHANGE_PAGE:
      return ({
        ...state,
        ...action.value
      });
    case SORT_PRODUCTS:
      return ({
        ...state,
        order: action.value
      });
    case SEARCH_PRODUCT_BY_NAME:
      return ({
        ...state,
        search: action.value
      });
    default:
      return state;
  }
}