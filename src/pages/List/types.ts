export type RowData = {
  name: string,
  quantity: number,
  date: Date,
  id: number,
  description: string
};

export type OrderField = 'id'|'quantity'|'date';

export type OrderDirection = 'asc'|'desc';

export interface Order {
  field: OrderField,
  direction: OrderDirection
}

export type State = {
  loading: boolean,
  success: boolean,
  error: boolean,
  data: Array<RowData>,
  errorMessage?: string,
  order: Order,
  limit: number,
  skip: number,
  search?: string,
  count: number,
  rowsPerPage: number,
  page: number
};

export type Action = {
  type?: string,
  value?: State,
};

export type GetProductsDataArguments = {
  dispatch: Function,
  order: Order,
  limit: number,
  skip: number,
  search?: string
};