import {
  POST_PRODUCTS_ERROR,
  POST_PRODUCTS_START,
  POST_PRODUCTS_SUCCESS,
  CLOSE_FORM,
  RESET_FORM
} from '../../actionsTypes';
import { FormFetching } from './types';

type Action = {
  type?: string,
  [key: string]: any
};

type State = FormFetching;

const INITIAL_STATE = {
  loading: false,
  error: false,
  success: false,
  close: false
};

export function addNewProduct(state: State = INITIAL_STATE, action: Action = {}) {
  switch (action.type) {
    case POST_PRODUCTS_START:
    case POST_PRODUCTS_SUCCESS:
    case POST_PRODUCTS_ERROR:
      return ({
        ...state,
        ...action.value
      });
    case CLOSE_FORM:
      return ({
        ...state,
        close: true
      });
    case RESET_FORM:
      return INITIAL_STATE;
    default:
      return state;
  }
}