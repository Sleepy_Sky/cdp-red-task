import { FormEvent, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { FormFetching } from './types';
import { State } from '../List/types';
import { FormValues } from '../../utils/CreateEditProductForm/types';
import { postProducts, closeForm, resetForm } from './actions';
import { getProductsData } from '../List/actions';

type Store = {
  addNewProduct: FormFetching,
  listState: State
};

export function useSubmit(): { onSubmit: Function, store: FormFetching } {
  const dispatch = useDispatch();
  const { listState, addNewProduct } = useSelector((store: Store) => store);

  const onSubmit = async (event: FormEvent<HTMLFormElement>, value: FormValues) => {
    await postProducts(event, value, dispatch);
    await getProductsData({
      search: listState.search,
      order: listState.order,
      skip: listState.skip,
      limit: listState.limit,
      dispatch: dispatch
    });
    closeForm(dispatch);
  };

  useEffect(() => {
    return () => resetForm(dispatch);
  }, []);

  return { store: addNewProduct, onSubmit };
}