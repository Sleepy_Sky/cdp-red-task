export type FormFetching = {
  loading: boolean,
  error: boolean,
  success: boolean,
  close: boolean,
  data?: object,
  errorMessage?: string
};