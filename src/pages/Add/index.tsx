import React from 'react';
import { CreateEditProductForm } from '../../utils/CreateEditProductForm';
import { useSubmit } from './useSubmit';

export function AddProductDialog() {
  const { onSubmit, store } = useSubmit();

  return (
    <CreateEditProductForm
      error={store.error}
      errorMessage={store.errorMessage}
      loading={store.loading}
      initialValues="default"
      title="Add New Product"
      onSubmit={onSubmit}
      closeForm={store.close}
    />
  );
}