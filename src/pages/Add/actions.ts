import Moment from 'moment';
import { FormEvent } from 'react';
import { fetcher } from '../../utils/fetcher';
import { FormValues } from '../../utils/CreateEditProductForm/types';
import { API_PRODUCTS } from '../../config/endpoints';
import {
  POST_PRODUCTS_ERROR,
  POST_PRODUCTS_START,
  POST_PRODUCTS_SUCCESS,
  CLOSE_FORM,
  RESET_FORM
} from '../../actionsTypes';

export function resetForm(dispatch: Function) {
  dispatch({
    type: RESET_FORM
  });
}

export function closeForm(dispatch: Function) {
  dispatch({
    type: CLOSE_FORM
  });
}

export async function postProducts(event: FormEvent, data: FormValues, dispatch: Function) {
  event.preventDefault();

  const properDataTypes = {
    name: data.name,
    quantity: data.quantity,
    description: data.description,
    date: Moment(data.date).toDate(),
    contactEmail: data.email
  };

  try {
    dispatch({
      type: POST_PRODUCTS_START,
      value: {
        loading: true
      }
    });

    const data = await fetcher.post({
      url: API_PRODUCTS,
      body: properDataTypes
    });

    dispatch({
      type: POST_PRODUCTS_SUCCESS,
      value: {
        loading: false,
        success: true,
        data
      }
    })

  } catch (error) {
    dispatch({
      type: POST_PRODUCTS_ERROR,
      value: {
        loading: false,
        error: true,
        errorMessage: error.message
      }
    })
  }
}