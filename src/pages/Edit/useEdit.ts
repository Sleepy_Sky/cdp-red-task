import Momen from 'moment';
import { useEffect, FormEvent } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useRouteMatch } from 'react-router-dom';
import { getProductAction, resetForm, editProductData } from './actions';
import { ReducerStore, PrevData } from './types';
import { State } from '../List/types';
import { FormValues } from '../../utils/CreateEditProductForm/types';

type Store = {
  editProduct: ReducerStore,
  listState: State
};

type HookReturn = {
  prevData?: PrevData,
  onSubmit: Function,
  store: ReducerStore
};

export function useEdit(): HookReturn {
  const { listState, editProduct } = useSelector((store: Store) => store);
  const dispatch = useDispatch();
  const match: { params: { id: string } } = useRouteMatch();

  const onSubmit = async (event: FormEvent<HTMLFormElement>, value: PrevData) => {
    event.preventDefault();

    const properData = {
      ...value,
      date: Momen(value.date).toDate(),
    };

    await editProductData(properData, dispatch);

  };


  useEffect(() => {
    getProductAction(match.params.id, dispatch);
    return () => {
      resetForm(dispatch);
    };
  }, []);

  return { prevData: editProduct.prevData, onSubmit, store: editProduct };
}