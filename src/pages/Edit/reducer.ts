import {
  GET_PRODUCT,
  RESET_FORM,
  PUT_PRODUCT_START,
  PUT_PRODUCT_SUCCESS,
  PUT_PRODUCT_ERROR
} from '../../actionsTypes';
import { ReducerAction, ReducerStore } from './types';

const INITIAL_STORE = {
  loading: false,
  success: false,
  error: false,
  errorMessage: ''
};

export function editProduct(store: ReducerStore = INITIAL_STORE, action: ReducerAction = {}) {
  switch (action.type) {
    case PUT_PRODUCT_START:
    case PUT_PRODUCT_SUCCESS:
    case PUT_PRODUCT_ERROR:
      return ({
        ...store,
        ...action.value
      });
    case GET_PRODUCT:
      return ({
        ...store,
        prevData: action.value
      });
    case RESET_FORM:
      return INITIAL_STORE;
    default:
      return store;
  }
}
