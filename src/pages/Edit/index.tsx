import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useEdit } from './useEdit';
import { CreateEditProductForm } from '../../utils/CreateEditProductForm';

export function EditProductDialog() {
  const { prevData, onSubmit, store } = useEdit();

  return prevData ? (
    <CreateEditProductForm
      errorMessage={store.errorMessage}
      editForm
      title={`Editing: ${prevData?.name}`}
      onSubmit={onSubmit}
      initialValues={prevData}
      loading={store.loading}
      error={store.error}
      closeForm={false}
    />
  ) : (
    <Dialog open>
      <CircularProgress />
    </Dialog>
  )
}