import Moment from 'moment';
import { fetcher } from '../../utils/fetcher';
import {
  GET_PRODUCT,
  RESET_FORM,
  PUT_PRODUCT_START,
  PUT_PRODUCT_SUCCESS,
  PUT_PRODUCT_ERROR
} from '../../actionsTypes';
import { API_PRODUCTS } from '../../config/endpoints';
import { GetProductResponse } from './types';

export async function editProductData(data: GetProductResponse, dispatch: Function) {
  try {
    dispatch({
      type: PUT_PRODUCT_START,
      value: {
        loading: true
      }
    });

    const response = await fetcher.put({ url: `${API_PRODUCTS}/${data.id}`, body: data });
    console.log(response);


  } catch (error) {
    dispatch({
      type: PUT_PRODUCT_ERROR,
      value: {
        loading: false,
        error: true,
        errorMessage: error.message
      }
    });
  }
}


export function resetForm(dispatch: Function) {
  dispatch({
    type: RESET_FORM
  });
}

export async function getProductAction(id: number|string, dispatch: Function) {
  try {
    const response: GetProductResponse = await fetcher.get({ url: `${API_PRODUCTS}/${id}` });

    const prevData = {
      name: response.name,
      date: Moment(response.date).format('YYYY-MM-DD'),
      quantity: response.quantity,
      description: response.description,
      id: response.id
    };

    dispatch({
      type: GET_PRODUCT,
      value: prevData
    });
  } catch (error) {
    console.error(error);
  }
}