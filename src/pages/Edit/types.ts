export type ReducerAction = {
  type?: string,
  value?: any
};

export type GetProductResponse = {
  name: string,
  quantity: number,
  date: Date,
  description: string,
  id: number
};

export type PrevData = {
  name: string,
  quantity: number,
  date: string,
  description: string,
  id: number
};

export interface ReducerStore {
  prevData?: PrevData,
  loading: boolean,
  success: boolean,
  error: boolean,
  errorMessage: string
}