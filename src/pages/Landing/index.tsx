import React from 'react'
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
    width: '100vw',
    backgroundColor: theme.palette.primary.main,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  infoContainer: {
    width: '340px',
    display: 'grid',
    gridTemplateRows: 'repeat(4, 100px)',
    alignItems: 'center',
    justifyItems: 'center'
  }
}));

export default function () {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.infoContainer}>
        <Typography variant="h1">
          HELLO!
        </Typography>
        <Typography variant="body1">
          This is app created for CD Project Red as a part of recruitment process. I hope You'll enjoy it.
        </Typography>
        <Typography variant="body1">
          ps. I would be very grateful if you keep in mind I'm not familiar with TypeScript. In fact, it's first time I use it in a more-serious-project. Cheers!
        </Typography>
        <Button component={Link} to="/products" variant="contained" color="secondary">
          Get Started
        </Button>
      </div>
    </div>
  );
}