import { useState, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { deleteProductAction } from './actions';
import { State } from './reducer'
import { State as ListState } from '../List/types';
import { getProductsData } from '../List/actions';

type Store = {
  deleteProductState: State,
  listState: ListState
}

export function useDelete(): { infoHandler: boolean, closeDialog: boolean, onClickDelete: Function, message: string } {
  const [infoHandler, setInfoHandler] = useState(false);
  const [closeDialog, setCloseDialog] = useState(false);
  const closeDialogRef = useRef(closeDialog);
  closeDialogRef.current = closeDialog;
  const dispatch = useDispatch();
  const store = useSelector((store: Store) => store);

  const onClickDelete = async (id: number|string) => {
    setInfoHandler(true);

    const { listState } = store;

    const reloadData = {
      order: listState.order,
      limit: listState.limit,
      search: listState.search,
      skip: listState.skip,
      dispatch: dispatch
    };

    await deleteProductAction(id,  dispatch);
    await getProductsData(reloadData);

    setTimeout(() => {
      setCloseDialog(true);
    }, 1000);
  };

  return { infoHandler, closeDialog, onClickDelete, message: store.deleteProductState.value?.message };
}