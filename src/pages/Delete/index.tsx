import React, { useState } from 'react';
import { useRouteMatch, Link, Redirect } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { useDelete } from './useDelete';

const useStyles = makeStyles(theme => ({
  message: {
    margin: theme.spacing(2)
  }
}));

export function DeleteDialog() {
  const classes = useStyles();
  const { infoHandler, closeDialog, onClickDelete, message } = useDelete();

  const match: { params: { id: string } } = useRouteMatch();

  if (closeDialog) return <Redirect to="/products" />;

  return (
    <Dialog open>
      {infoHandler ? (
        <div className={classes.message}>
          <Typography variant="body1">
            {message}
          </Typography>
        </div>
      ) : (
        <>
          <DialogTitle>
            Delete Product
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              Please confirm you're sure about delete such interesting product.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button component={Link} to="/products" color="primary" >
              Cancel
            </Button>
            <Button color="primary" onClick={() => onClickDelete(match.params.id)} >
              Confirm
            </Button>
          </DialogActions>
        </>
      )}
    </Dialog>
  );
}