import { DELETE_PRODUCT } from '../../actionsTypes';

export type State = {
  value: {
    message: string,
    success?: boolean
  }
}

type Action = {
  type?: string,
  value?: State
};

const INITIAL_STATE = {
  value: {
    message: ''
  }
};

export function deleteProductState(state: State = INITIAL_STATE, action: Action) {
  switch (action.type)  {
    case DELETE_PRODUCT:
      return ({
        value: action.value
      });
    default:
      return state;
  }
}