import { fetcher } from '../../utils/fetcher';
import { API_PRODUCTS } from '../../config/endpoints';
import { DELETE_PRODUCT } from '../../actionsTypes';

export async function deleteProductAction(id: number|string, dispatch: Function) {
  try {
    const url = `${API_PRODUCTS}/${id}`;

    const response: { count: number } = await fetcher.delete({ url });

    if (response.count === 1) {
      dispatch({
        type: DELETE_PRODUCT,
        value: { success: true, message: 'Product was successfully slain!' }
      });
    } else if (response.count === 0) {
      dispatch({
        type: DELETE_PRODUCT,
        value: { success: true, message: 'Product has been slain before!' }
      });
    } else {
      dispatch({
        type: DELETE_PRODUCT,
        value: { success: true, message: 'I don\'t know what\'s happened but no error occurred.' }
      });
    }

  } catch (error) {
    console.error(error);
  }
}

