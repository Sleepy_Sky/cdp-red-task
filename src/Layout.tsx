import React from 'react';
import { Link, Route, useRouteMatch } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import House from '@material-ui/icons/House';
import { ListContainer } from './pages/List'
import { AddProductDialog } from './pages/Add';
import { DeleteDialog } from './pages/Delete';
import { EditProductDialog } from './pages/Edit';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
    width: '100%',
    display: 'grid',
    gridTemplateRows: '45px 1fr',
    alignItems: 'start',
    justifyItems: 'center'
  },
  appBar: {
    display: 'flex',
    justifyContent: 'space-between',
    height: '45px',
    minHeight: '45px'
  },
  content: {
    width: '100%',
    height: '100%',
    maxWidth: '1200px',
    padding: theme.spacing(2)
  }
}));

export default function() {
  const classes = useStyles();
  const match = useRouteMatch();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar classes={{ root: classes.appBar }}>
          <Typography variant="h4">
            Products List
          </Typography>
          <IconButton component={Link} to="/" color="inherit">
            <House />
          </IconButton>
        </Toolbar>
      </AppBar>
      <div className={classes.content}>
        <Route path={match.url} component={ListContainer} />
        <Route path={`${match.url}/add`} component={AddProductDialog} />
        <Route path={`${match.url}/delete/:id`} component={DeleteDialog} />
        <Route path={`${match.url}/edit/:id`} component={EditProductDialog} />
      </div>
    </div>
  );
}
