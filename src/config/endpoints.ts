const API: string = 'https://0wrqhygp.cdprojektred.com/api';

export const API_PRODUCTS = `${API}/Products`;
export const API_PRODUCTS_FIND_ONE = `${API}/Products/findOne`;
export const API_PRODUCTS_COUNT = `${API}/Products/count`;
