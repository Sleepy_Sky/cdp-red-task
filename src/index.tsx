import React, { Suspense, lazy } from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';
import { MuiThemeConfiguration } from './utils/MuiThemeConfiguration';
import { LazyCircularProgress } from './utils/LazyCircularProgress';
import { combinedReducers } from './reducers';

const LandingPage = lazy(() => import('./pages/Landing'));
const Layout = lazy(() => import('./Layout'));

const store = createStore(combinedReducers);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <MuiThemeConfiguration>
        <Suspense fallback={<LazyCircularProgress />}>
          <Route exact path="/" component={LandingPage} />
          <Route path="/products" component={Layout} />
        </Suspense>
      </MuiThemeConfiguration>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'));

