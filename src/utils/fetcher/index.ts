import { FetcherGetDelete, FetcherPostPull } from './types';

async function preFetch(url: string, method: string, headers?: HeadersInit, body?: object) {
  const response = await fetch(url, {
    method,
    headers: new Headers({
      'Content-Type': 'application/json',
      ...headers
    }),
    body: JSON.stringify(body)
  });

  const data = await response.json();

  if(!!data.error) throw Error(data.error.message);

  return data;
}

function postFetch({ body, headers, url }: FetcherPostPull) {
  return preFetch(url, 'POST', headers, body);
}

function getFetch({ headers, url }: FetcherGetDelete) {
  return preFetch(url, 'GET', headers);
}

function deleteFetch({ headers, url }: FetcherGetDelete) {
  return preFetch(url, 'DELETE', headers);
}

function putFetch({ body, headers, url }: FetcherPostPull) {
  return preFetch(url, 'PUT', headers, body);
}

export const fetcher = {
  post: postFetch,
  get: getFetch,
  delete: deleteFetch,
  put: putFetch
};
