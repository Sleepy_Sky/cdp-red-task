export interface FetcherPostPull {
  url: string,
  body: object,
  headers?: HeadersInit
}

export interface FetcherGetDelete {
  url: string,
  headers?: HeadersInit
}