import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress'

import { useFormData } from './useFormData';
import { initialValues } from './types';

interface Props {
  title: string,
  onSubmit: Function,
  initialValues: initialValues,
  loading: boolean,
  error: boolean
  errorMessage?: string,
  closeForm: boolean,
  editForm?: boolean
}

const useStyles = makeStyles(theme => ({
  dialogPaper: {
    width: '100%',
    minWidth: '300px',
    maxWidth: '500px',
    padding: theme.spacing(2)
  },
  form: {
    width: '100%',
    display: 'grid',
    gridGap: theme.spacing(2),
    alignItems: 'center',
    justifyItems: 'center'
  },
  buttonAndInfoContainer: {
    height: '100%',
    width: '100%',
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    position: 'absolute',
    right: '10px'
  },
  fetchInfo: {
    position: 'absolute',
    left: '10px'
  }
}));

export function CreateEditProductForm(props: Props) {
  const {
    title,
    onSubmit,
    initialValues,
    loading,
    error,
    errorMessage,
    closeForm,
    editForm = false
  } = props;
  const classes = useStyles();
  const [handleRedirect, setHandleRedirect] = useState(false);
  const { values, isSubmitAllowed, validation, handleInput, handleChange } = useFormData(initialValues, editForm);

  if (handleRedirect || closeForm) {
    return <Redirect to="/products" />;
  }

  const gridRows = editForm ? 'repeat(5, 60px)' : 'repeat(6, 60px)';

  return (
    <Dialog
      classes={{
        paper: classes.dialogPaper
      }}
      scroll="body"
      open
      onClose={() => setHandleRedirect(true)}
    >
      <DialogTitle>{title}</DialogTitle>
      <form
        style={{ gridTemplateRows: gridRows }}
        className={classes.form}
        onSubmit={e => onSubmit(e, values)}
      >
        <TextField
          value={values.name}
          fullWidth
          label="Product Name"
          onChange={e => handleChange.name(e)}
          error={validation.name.error}
          helperText={validation.name.helper}
        />
        <TextField
          value={values.quantity || ''}
          fullWidth
          label="Quantity"
          type="number"
          onInput={e => handleInput.quantity(e)}
          onChange={e => handleChange.quantity(e)}
          error={validation.quantity.error}
          helperText={validation.quantity.helper}
        />
        <TextField
          value={values.date}
          fullWidth
          label="Date"
          type="date"
          onChange={e => handleChange.date(e)}
          error={validation.date.error}
          helperText={validation.date.helper}
        />
        {!editForm && (<TextField
          value={values.email}
          fullWidth
          label="Email"
          onChange={e => handleChange.email(e)}
          error={validation.email.error}
          helperText={validation.email.helper}
        />)}
        <TextField
          value={values.description}
          fullWidth
          label="Description"
          onChange={e => handleChange.description(e)}
          error={validation.description.error}
          helperText={validation.description.helper}
        />
        <div className={classes.buttonAndInfoContainer}>
          <Button
            className={classes.button}
            type="submit"
            color="primary"
            variant="contained"
            disabled={!isSubmitAllowed}
          >
            submit
          </Button>
          {loading && <CircularProgress className={classes.fetchInfo} />}
          {error && (
            <Typography className={classes.fetchInfo} title={errorMessage} variant="caption">
              Error Occurred
            </Typography>
          )}
        </div>
      </form>
    </Dialog>
  );
}