import { useState, useEffect, ChangeEvent } from 'react';
import Moment from 'moment';
import { fetcher } from '../fetcher';
import { API_PRODUCTS_FIND_ONE } from '../../config/endpoints';
import { FormValues, initialValues } from './types';

interface ValidationItem { error: boolean, helper: string }

interface Validation {
  name: ValidationItem,
  quantity: ValidationItem,
  date: ValidationItem,
  email: ValidationItem,
  description: ValidationItem
}

interface HookReturn {
  handleChange: {
    name: Function,
    quantity: Function,
    date: Function,
    email: Function,
    description: Function
  },
  handleInput: {
    quantity: Function
  },
  validation: Validation,
  values: FormValues,
  isSubmitAllowed: boolean
}

const DEFAULT_VALIDATION = {
  name: { error: false, helper: 'You have to enter at least 5 characters.' },
  quantity: { error: false, helper: 'Enter a number.' },
  date: { error: false, helper: '' },
  email: { error: false, helper: 'Enter an email.' },
  description: { error: false, helper: '' }
};

const DEFAULT_FORM_VALUES = {
  name: '',
  quantity: NaN,
  date: Moment().format('YYYY-MM-DD'),
  email: '',
  description: ''
};

export function useFormData(initialValues: initialValues, editForm: boolean): HookReturn {
  const [validation, setValidation] = useState(DEFAULT_VALIDATION);
  const initialValuesState = initialValues === 'default' ? DEFAULT_FORM_VALUES : initialValues;
  const [values, setValues] = useState(initialValuesState);
  const [isSubmitAllowed, setIsSubmitAllowed] = useState(false);

  const handleNameChange = async (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    let isNameExists: boolean;

    try {
      const query = JSON.stringify({ where: { name: value } });
      const urlWithQuery =
        `${API_PRODUCTS_FIND_ONE}?filter=${encodeURIComponent(query)}`;

      await fetcher.get({ url: urlWithQuery });

      isNameExists = true;
    } catch {
      isNameExists = false;
    }

    const noSpacesValue = value.replace(/\s/g, '');

    if (noSpacesValue.length < 5 && noSpacesValue.length > 0) {
      setValidation({
        ...validation,
        name: { error: true, helper: 'You have to enter at least 5 characters.' }
      });
    } else if (noSpacesValue.length === 0) {
      setValidation({
        ...validation,
        name: { error: true, helper: 'This field is required.' }
      });
    } else if (isNameExists) {
      setValidation({
        ...validation,
        name: { error: true, helper: 'A product with this name already exists.' }
      });
    } else {
      setValidation({
        ...validation,
        name: { error: false, helper: '' }
      });
    }

    setValues({
      ...values,
      name: value
    });
  };

  const handleQuantityChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;

    setValues({
      ...values,
      quantity: parseInt(value, 10)
    });
  };

  const handleQuantityInput = (event: ChangeEvent<HTMLInputElement>) => {
    const { valid } = event.target.validity;

    if (!valid) {
      setValidation({
        ...validation,
        quantity: { error: true, helper: 'You have to enter a number.' }
      });
    } else {
      setValidation({
        ...validation,
        quantity: { error: false, helper: '' }
      });
    }
  };

  const handleDateChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;

    if (value.length !== 0) {
      setValues({
        ...values,
        date: value
      });
    }
  };

  const handleEmailChange = (event: ChangeEvent<HTMLInputElement>) => {
    const re = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;

    const { value } = event.target;

    const isValidEmail = re.test(String(value).toLowerCase());

    if (!isValidEmail) {
      setValidation({
        ...validation,
        email: { error: true, helper: 'Incorrect email address.' }
      });
    } else {
      setValidation({
        ...validation,
        email: { error: false, helper: '' }
      });
    }

    if (editForm) {
      setValidation({
        ...validation,
        email: { error: false, helper: '' }
      });
    }

    setValues({
      ...values,
      email: value
    });
  };

  const handleDescriptionChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;

    setValues({
      ...values,
      description: value
    });
  };

  useEffect(() => {
    const submitValidation = () => {
      const errors =
        Object.entries(validation).map(element => Object.entries(element[1])[0][1]);

      if (errors.indexOf(true) === -1 && !!values.name && !!values.quantity && (!!values.email || editForm)) {
        setIsSubmitAllowed(true);
      } else {
        setIsSubmitAllowed(false);
      }
    };

    submitValidation();
  }, [validation, values.name, values.quantity, values.email]);

  return {
    values,
    validation,
    isSubmitAllowed,
    handleChange: {
      name: handleNameChange,
      quantity: handleQuantityChange,
      date: handleDateChange,
      description: handleDescriptionChange,
      email: handleEmailChange
    },
    handleInput: {
      quantity: handleQuantityInput
    }
  };
}