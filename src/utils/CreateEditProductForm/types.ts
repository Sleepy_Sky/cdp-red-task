export interface FormValues {
  name?: string,
  quantity?: number,
  date?: string,
  email?: string,
  description?: string,
}

interface FormInitialValues {
  name: string,
  quantity: number,
  date: string,
  email?: string,
  description: string,
}

export type initialValues = FormInitialValues|'default';