// post products actions
export const POST_PRODUCTS_START = 'POST_PRODUCTS_START';
export const POST_PRODUCTS_SUCCESS = 'POST_PRODUCTS_SUCCESS';
export const POST_PRODUCTS_ERROR = 'POST_PRODUCTS_ERROR';
// get products actions
export const GET_PRODUCTS_START = 'GET_PRODUCTS_START';
export const GET_PRODUCTS_SUCCESS = 'GET_PRODUCTS_SUCCESS';
export const GET_PRODUCTS_ERROR = 'GET_PRODUCTS_ERROR';
// sort products
export const SORT_PRODUCTS = 'SORT_PRODUCTS';
export const SEARCH_PRODUCT_BY_NAME = 'SEARCH_PRODUCT_BY_NAME';
// list actions
export const CHANGE_ROWS_PER_PAGE = 'CHANGE_ROWS_PER_PAGE';
export const CHANGE_PAGE = 'CHANGE_PAGE';
// delete
export const DELETE_PRODUCT = 'DELETE_PRODUCT';
// form actions
export const CLOSE_FORM = 'CLOSE_FORM';
export const RESET_FORM = 'RESET_FORM';
// put product
export const PUT_PRODUCT_START = 'PUT_PRODUCT_START';
export const PUT_PRODUCT_SUCCESS = 'PUT_PRODUCT_SUCCESS';
export const PUT_PRODUCT_ERROR = 'PUT_PRODUCT_ERROR';
// get product
export const GET_PRODUCT = 'GET_PRODUCT';
