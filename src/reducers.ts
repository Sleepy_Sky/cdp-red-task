import { combineReducers } from 'redux';
import { addNewProduct } from './pages/Add/reducer';
import { listState } from './pages/List/reducer';
import { deleteProductState } from './pages/Delete/reducer';
import { editProduct } from './pages/Edit/reducer';

export const combinedReducers = combineReducers({
  addNewProduct,
  listState,
  deleteProductState,
  editProduct
});